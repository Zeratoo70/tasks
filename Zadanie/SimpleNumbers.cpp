#include "Functions.h"

using namespace std;
typedef unsigned int UI;

void simpleNum(UI, UI);

void simpleNum(UI M, UI N)
{
	UI tempVar;

	if (M < 2)
		cout << "Error! Make M a little bit greater. Min M value is 2" << endl;
	else
	{
		tempVar = M;
		M = 2;
	}

	if (N > 300000)
	{
		cout << "Error! Make N a little bit lesser. Max N value is 300000" << endl;
	}


	if (M >= 2 && N <= 300000)
	{
		bool* isSimple = new bool[N + 1];
		bool flag = false;

		for (UI i = M; i < N + 1; i++)
		{
			isSimple[i] = true;
		}

		for (UI i = M; i*i <= N; i++)
		{
			for (UI j = i * i; j <= N; j += i)
			{
				isSimple[j] = false;
			}
		}

		for (UI i = tempVar; i < N + 1; i++)
		{
			if (isSimple[i] == true)
			{
				flag = true;
				cout << i << endl;
			}
		}
		if (flag == false)
		{
			cout << "Absent" << endl;
		}
	}
}