#include "Functions.h"
using namespace std;

typedef unsigned int UI;


void points_input();
double points_minRange();
void points_inCirlce();
int points_inRing(double, double);


void simpleNum(UI, UI);

struct Point
{
	double x_coord, y_coord;
};

const int N = 5;
Point PointNum[N];

void Program()
{
	int var = 0;
	double minRad, maxRad;
	while ((var != 1 && var != 2 && var != 3))
	{
		cout << "1. ��������� 2 ����� �� ���������" << endl
			<< "2. ������������ ����� ����� � ���������� ������� R" << endl
			<< "3. ������������ ����� ����� � ������ � �������� �������� ��������" << endl;
		if (!(cin >> var))
		{
			cout << "������ �����!" << endl;
			break;
		}
	}
	switch (var)
	{
	case 1: 
		system("cls");
		points_input();
		points_minRange();
		break;
	case 2:
		system("cls");
		points_input();
		points_inCirlce();
		break;
	case 3:
		system("cls");
		points_input();
		cout << "\n������� ������� ������ ������� ����������, ����� �������" << endl;
		if (!(cin >> minRad >> maxRad))
		{
			cout << "\n������! ������ ������ ������� " << endl;
			break;
		}
		else
		{
			if (minRad > maxRad)
			{
				swap(minRad, maxRad);
			}
			cout << "������������ ���������� �����, ����������� � ������ �����: " << points_inRing(minRad, maxRad) << endl;
		}
		break;
	default:
		break;
	}
}
bool errorflag = true;
void points_input()
{
	
		for (int i = 0; i < N; i++)
	{
		cout << "������� ���������� X" << endl;

		if (!(cin >> PointNum[i].x_coord))
		{
			system("cls");
			errorflag = true;
			cout << "������ �����! ������� ������ ���������� X";
			break;
		}

		cout << "������� ���������� Y" << endl;
		if (!(cin >> PointNum[i].y_coord))
		{
			system("cls");
			errorflag = true;
			cout << "������ �����! ������� ������ ���������� Y";
			break;
		}

		else
		{
			errorflag = false;
			if (i == N - 1)
			{
				system("cls");
				cout << "��������� ���� ����������: \n\t\tX Y";
				for (int i = 0; i < N; i++)
				{
					cout << "\n����� " << i + 1 << ":\t" << PointNum[i].x_coord << " " << PointNum[i].y_coord;
				}
			}
		}
	}
}

double points_minRange()
{
	double PointRange[N][N];
	double X21, Y21, minVar;
	if (errorflag == false) 
	{
		system("cls");
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
			{

				X21 = PointNum[j].x_coord - PointNum[i].x_coord;
				Y21 = PointNum[j].y_coord - PointNum[i].y_coord;
				PointRange[i][j] = sqrt((X21*X21) + (Y21*Y21));
				if (PointRange[i][j] == PointRange[j][i])
				{
					continue;
				}
				cout << "���������� ����� ������� " << i + 1 << " � " << j + 1 << " = " << PointRange[i][j] << endl;
				minVar = PointRange[i][j];
			}
		}

		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
			{
				if ((minVar > PointRange[i][j]) && (i != j))
					swap(minVar, PointRange[i][j]);

			}
		}

		cout << "����������� ���������� ����� ����� ������� ����� " << minVar << endl;

		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
			{
				if (minVar == PointRange[i][j])
				{
					cout << "��� ��������� ����� ��� " << i + 1 << " � " << j + 1 << endl;
					break;
				}
			}
		}
	}
	return 0;
}

double point_radius(double x_coord, double y_coord)
{
	double point_rad = sqrt((x_coord * x_coord) + (y_coord * y_coord));
	return point_rad;
}

void points_inCirlce()
{
	double rad;
	double point_rad[N];
	int counter = 0;

	cout << "\n������� ������ ���������� � ������� ����� ������ �����" << endl;

	if(!(cin >> rad))
	{
		cout << "\n������! ������ ������ �������" << endl;
	}
	else 
	{
		for (int i = 0; i < N; i++)
		{
			point_rad[i] = point_radius(PointNum[i].x_coord, PointNum[i].y_coord);
			if (point_rad[i] <= rad)
			{
				counter++;
			}
		}
		cout << "������������ ���������� �����, ������� ����� ����� � ���������� ��������� ������� �����: " << counter << endl;
	}
}


int points_inRing(double minRad, double maxRad)
{
	double point_rad[N];
	int counter = 0;
	for (int i = 0; i < N; i++)
	{
		point_rad[i] = point_radius(PointNum[i].x_coord, PointNum[i].y_coord);
		if (point_rad[i] >= minRad && point_rad[i] <= maxRad)
		{
			counter++;
		}
	}

	return counter;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	Program(); // ������������� ��������� �� ������� ������ ������; ������ ������ � ����� SimpleNumbers.cpp
	//simpleNum(2, 100); 
	_getch();
	return 0;
}